## Base Front End Project

### Install

`npm install`

### Gulp Actions
**Default**

`gulp`

**Watch**

`gulp watch`

**Clean Dist**

`gulp clean`

### Publish for production

Change `devBuild` value on file `gulpgile.js`

`devBuild = false`